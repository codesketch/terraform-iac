# VPC with Autoscaling and Load-Balancer  

The configuration provided on this repository creates a:

 * VPC with a [configurable](./terraform/aws/variables.tf) CIDR and public/private availability zones.
 * A Load Balancer connected to an autoscaling group which when needed will scale Up or Down EC2 instances.  
 The number of instances part of the autoscaling group is [configurable](./terraform/aws/variables.tf) and defined as a default between 1 and 3.  
 Each EC2 instance part of the autoscaling group is provisioned via bootstrapping with Apache 2 and is provided access to an S3 bucket from wich static contents can be served and acess to a DynamoDB Table.
 The aoutoscaling group will react to the following rules:
    - Scale UP if Averge CPU usage is higher than 80%  
    - Scale DOWN if Average CPU usage is less than 60%  

The configuration and repository structure is created so that multiple Cloud Providers and multiple environments can be supported.

Multiple cloud providers are supported employing a differentiate directory structure i.e. `terraform/aws` for aws `terraform/gcp` for Google Cloud Provider.

Multiple environment are supported employing terraform workspaces.

This repository assumes that terraform state files are committed and maintained as part of this repository, as such no remote state storage is configured, for more information see [Terraform Backends](https://www.terraform.io/docs/language/settings/backends/remote.html) 

# Alternative

An alternative to the proposed solution is provided on `terraform/aws_s3_website` that create an S3/CloufFront solution that requires less maintenance effort and potentially improve performances serving contents from a geographic near-user location.

This alternative solution can be easilly extended adopting AWS for improved security.

On this solution of the infrastructure maintenance effort, i.e. autoscaling, is delegated to AWS allowing DevOps teams to concentrate on the business needs and requirements potentially increasing the team velocity.  

## Usage  

To run this example you need to execute:

```bash
$ sh execute <action> <provider> <environment> [config_file_path] 
```

Where:  

* **action** is one of the terraform allowed commands i.e. plan, apply
* **provider** is one of the supported providers, as of now only aws
* **environment** the deployment environment 
* **config_file_path** (optional) is the path to a configuration file relative to the current directory  

### Example

```bash
$ sh execute plan aws development ./resources/dev.config.tfvars.json
```

```bash
$ sh execute plan aws_s3_website development ./resources/dev.config.tfvars.json
```

The execute script will:

 * Create, if required, and select the environment workspace
 * Initialise terraform
 * Execute the required action

# Basic Solution Docs

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >=3.19.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >=3.19.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_acm"></a> [acm](#module\_acm) | terraform-aws-modules/acm/aws | ~> 2.0 |
| <a name="module_autoscaling"></a> [autoscaling](#module\_autoscaling) | terraform-aws-modules/autoscaling/aws | 3.9.0 |
| <a name="module_elb_http"></a> [elb\_http](#module\_elb\_http) | terraform-aws-modules/elb/aws | ~> 2.0 |
| <a name="module_http_sg"></a> [http\_sg](#module\_http\_sg) | terraform-aws-modules/security-group/aws//modules/https-443 | ~> 3.0 |
| <a name="module_security_group"></a> [security\_group](#module\_security\_group) | terraform-aws-modules/security-group/aws | ~> 3.0 |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | terraform-aws-modules/vpc/aws |  |

## Resources

| Name | Type |
|------|------|
| [aws_autoscaling_policy.cpu-policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy) | resource |
| [aws_autoscaling_policy.cpu-policy-scaledown](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy) | resource |
| [aws_cloudwatch_metric_alarm.example-cpu-alarm](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.example-cpu-alarm-scaledown](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_dynamodb_table.accounts-dynamodb-table](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table) | resource |
| [aws_iam_instance_profile.ec2_instance_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_policy.contents_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy_attachment.contents_policy_attach](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy_attachment) | resource |
| [aws_iam_role.ec2_access_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_route53_zone.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_zone) | resource |
| [aws_s3_bucket.contents](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_s3_bucket.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket) | resource |
| [aws_security_group.accounts_table_access_sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_ami.amazon_linux](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_elb_service_account.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/elb_service_account) | data source |
| [aws_iam_policy_document.contents](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.contents_assume_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.logs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_asg_desired_capacity"></a> [asg\_desired\_capacity](#input\_asg\_desired\_capacity) | The number of ec2 instances running on the ASG | `number` | `1` | no |
| <a name="input_asg_max_size"></a> [asg\_max\_size](#input\_asg\_max\_size) | The maximum number of ec2 instances on the ASG | `number` | `3` | no |
| <a name="input_asg_min_size"></a> [asg\_min\_size](#input\_asg\_min\_size) | The minimum number of ec2 instances on the ASG | `number` | `1` | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | Defines the region that this VPC is created on | `string` | `"eu-west-1"` | no |
| <a name="input_cidr"></a> [cidr](#input\_cidr) | Defines the CIDR for this VPC is created on | `string` | `"10.20.0.0/16"` | no |
| <a name="input_domain_name"></a> [domain\_name](#input\_domain\_name) | The name of the domain the application is exposed under | `string` | `"test-qb.tf"` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Defines the environment that this VPC is created for | `string` | `"development"` | no |
| <a name="input_num_of_instances"></a> [num\_of\_instances](#input\_num\_of\_instances) | The number of ec2 instances to schedule | `number` | `1` | no |
| <a name="input_owner"></a> [owner](#input\_owner) | Defines the owner that this VPC | `string` | `"qdb"` | no |
| <a name="input_private_subnets_cidr"></a> [private\_subnets\_cidr](#input\_private\_subnets\_cidr) | Defines the CIDR for the private subnets created for this VPC is created on | `list` | <pre>[<br>  "10.20.0.0/18",<br>  "10.20.128.0/18",<br>  "10.20.192.0/18"<br>]</pre> | no |
| <a name="input_public_subnets_cidr"></a> [public\_subnets\_cidr](#input\_public\_subnets\_cidr) | Defines the CIDR for the public subnets created for this VPC is created on | `list` | <pre>[<br>  "10.20.64.0/18"<br>]</pre> | no |

## Outputs

No outputs.

# Alternative Solution Docs

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~>2.35.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~>2.35.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws.cloudfront"></a> [aws.cloudfront](#provider\_aws.cloudfront) | ~>2.35.0 ~>2.35.0 |
| <a name="provider_aws.main"></a> [aws.main](#provider\_aws.main) | ~>2.35.0 ~>2.35.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_main"></a> [main](#module\_main) | github.com/riboseinc/terraform-aws-s3-cloudfront-website |  |

## Resources

| Name | Type |
|------|------|
| [aws_acm_certificate.cert](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate) | resource |
| [aws_acm_certificate_validation.cert](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate_validation) | resource |
| [aws_route53_record.cert_validation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_route53_record.web](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) | resource |
| [aws_s3_bucket_object.website_files](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object) | resource |
| [aws_route53_zone.main](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allowed_ips"></a> [allowed\_ips](#input\_allowed\_ips) | n/a | `list` | <pre>[<br>  "0.0.0.0/0"<br>]</pre> | no |
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | Defines the region that this VPC is created on | `string` | `"eu-west-1"` | no |
| <a name="input_domain"></a> [domain](#input\_domain) | The domain name / . | `string` | `"domain.com"` | no |
| <a name="input_fqdn"></a> [fqdn](#input\_fqdn) | The fully-qualified domain name of the resulting S3 website. | `string` | `"info.domain.com"` | no |
| <a name="input_mime_types"></a> [mime\_types](#input\_mime\_types) | n/a | `map` | <pre>{<br>  "css": "text/css",<br>  "htm": "text/html",<br>  "html": "text/html",<br>  "ico": "image/x-icon",<br>  "jpg": "image/jpg",<br>  "js": "application/javascript",<br>  "json": "application/json",<br>  "map": "application/javascript",<br>  "png": "image/png",<br>  "ttf": "font/ttf"<br>}</pre> | no |
| <a name="input_upload_directory"></a> [upload\_directory](#input\_upload\_directory) | n/a | `string` | `"../../build/"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_acm_certificate_arn"></a> [acm\_certificate\_arn](#output\_acm\_certificate\_arn) | n/a |
| <a name="output_cloudfront_distribution_id"></a> [cloudfront\_distribution\_id](#output\_cloudfront\_distribution\_id) | n/a |
| <a name="output_cloudfront_domain"></a> [cloudfront\_domain](#output\_cloudfront\_domain) | n/a |
| <a name="output_cloudfront_hosted_zone_id"></a> [cloudfront\_hosted\_zone\_id](#output\_cloudfront\_hosted\_zone\_id) | n/a |
| <a name="output_route53_fqdn"></a> [route53\_fqdn](#output\_route53\_fqdn) | n/a |
| <a name="output_s3_bucket_id"></a> [s3\_bucket\_id](#output\_s3\_bucket\_id) | n/a |
| <a name="output_s3_domain"></a> [s3\_domain](#output\_s3\_domain) | n/a |
| <a name="output_s3_hosted_zone_id"></a> [s3\_hosted\_zone\_id](#output\_s3\_hosted\_zone\_id) | n/a |

Documentation has been generated with [terraform-docs](https://github.com/terraform-docs/terraform-docs)