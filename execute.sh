#! /bin/bash

ACTION=${1:-'plan'}
PROVIDER=${2:-'aws'}
ENVIRONMENT=${3:-'development'}
VAR_FILE=${4:-''}

echo "Setting internal variables"
SCRIPT_PATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
OPTIONS="-var environment=${ENVIRONMENT}"
if [[ "${VAR_FILE}" != "" ]]; then
  OPTIONS="${OPTIONS} -var-file=${SCRIPT_PATH}/${VAR_FILE}"
fi

echo "Accessing ./terraform/${PROVIDER} directory to execute ${ACTION} on ${PROVIDER}"
cd ./terraform/${PROVIDER}

echo "Try creating workspace for ${ENVIRONMENT} environment and select it"
terraform workspace new ${ENVIRONMENT} || true
terraform workspace select ${ENVIRONMENT}

echo "Initialise terraform structures and modules"
terraform init
echo "Executing ${ACTION} with ${OPTIONS}"
terraform ${ACTION} ${OPTIONS}
