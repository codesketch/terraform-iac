variable "fqdn" {
  description = "The fully-qualified domain name of the resulting S3 website."
  default     = "info.domain.com"
}

variable "domain" {
  description = "The domain name / ."
  default     = "domain.com"
}
variable "allowed_ips" {
  type    = list
  default = ["0.0.0.0/0"]
}

variable "upload_directory" {
  default = "../../build/"
}

variable "mime_types" {
  default = {
    htm  = "text/html"
    html = "text/html"
    css  = "text/css"
    ttf  = "font/ttf"
    js   = "application/javascript"
    map  = "application/javascript"
    json = "application/json"
    png  = "image/png"
    jpg  = "image/jpg"
    ico  = "image/x-icon"
  }
}

variable "aws_region" {
  type = string
  description = "Defines the region that is used for this deployment"
  default = "eu-west-1"
}