module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  cidr            = var.cidr
  azs             = ["${var.aws_region}a", "${var.aws_region}b", "${var.aws_region}c"]
  private_subnets = var.private_subnets_cidr
  public_subnets  = var.public_subnets_cidr

  enable_classiclink              = false
  enable_classiclink_dns_support  = false
  enable_dns_hostnames            = false
  enable_dns_support              = true
  assign_ipv6_address_on_creation = false

  # VPC endpoint for DynamoDB
  enable_dynamodb_endpoint = true
  # VPC endpoint for S3
  enable_s3_endpoint = true
  # VPC endpoint for API gateway
  enable_apigw_endpoint = false
  # VPC endpoint for KMS
  enable_kms_endpoint = false

  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false

  tags = {
    Name        = "${var.owner}-${var.environment}-vpc"
    Environment = "${var.environment}"
    Owner       = "${var.owner}"
    Schedule    = "None"
  }
}
