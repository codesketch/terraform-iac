data "aws_iam_policy_document" "contents" {
  statement {
    actions = [
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::${var.environment}-contents/*",
    ]
  }
}

resource "aws_iam_policy" "contents_policy" {
  name        = "${var.environment}-contents-policy"
  policy      = data.aws_iam_policy_document.logs.json
}

resource "aws_s3_bucket" "contents" {
  bucket        = "${var.environment}-contents"
  acl           = "private"
  policy        = data.aws_iam_policy_document.logs.json
  force_destroy = true
}