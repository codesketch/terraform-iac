data "aws_iam_policy_document" "contents_assume_role" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    principals {
      type = "Service"
       identifiers = ["ec2.amazonaws.com"]
     }

    resources = [
      "arn:aws:s3:::${var.environment}-contents/*",
    ]
  }

  statement {
    effect = "Allow"
    actions = [
      "dynamodb:*"
    ]
    resources = [
      aws_dynamodb_table.accounts-dynamodb-table.arn
    ]
  }
}

resource "aws_iam_role" "ec2_access_role" {
  name               = "${var.environment}-s3-role"
  assume_role_policy = data.aws_iam_policy_document.contents_assume_role.json
}

resource "aws_iam_policy_attachment" "contents_policy_attach" {
  name       = "${var.environment}_contents_policy_attach"
  roles      = [aws_iam_role.ec2_access_role.name]
  policy_arn = aws_iam_policy.contents_policy.arn
}