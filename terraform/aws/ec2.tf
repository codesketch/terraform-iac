data "aws_ami" "amazon_linux" {
  most_recent = true

  owners = ["amazon"]

  filter {
    name = "name"

    values = [
      "amzn-ami-hvm-*-x86_64-gp2",
    ]
  }

  filter {
    name = "owner-alias"

    values = [
      "amazon",
    ]
  }
}

resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name  = "${var.environment}_ec2_profile"
  role = aws_iam_role.ec2_access_role.name
}

# module "ec2_instances" {
#   source  = "terraform-aws-modules/ec2-instance/aws"
#   version = "2.17.0"

#   instance_count = var.num_of_instances

#   name          = "${var.environment}-ec2-instance"
#   ami           = data.aws_ami.amazon_linux.id
#   instance_type = "t2.micro"
#   subnet_id     = var.private_subnets_cidr[0]
#   vpc_security_group_ids      = [module.security_group.this_security_group_id]
#   associate_public_ip_address = true
#   iam_instance_profile = aws_iam_instance_profile.ec2_instance_profile.name
#   user_data = "${file("${path.cwd}/bin/bootstrap_apache_install.sh")}"

#   root_block_device = [
#     {
#       volume_type = "gp2"
#       volume_size = 10
#     }
#   ]

#   tags = {
#     "Env"      = var.environment
#   }
# }