resource "aws_dynamodb_table" "accounts-dynamodb-table" {
  name           = "${var.environment}-accounts"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "AccountId"
  range_key      = "AccountName"

  attribute {
    name = "AccountId"
    type = "S"
  }

  attribute {
    name = "AccountName"
    type = "S"
  }

  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }

  tags = {
    Name        = "accounts"
    Environment = var.environment
  }
}