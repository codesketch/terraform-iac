# scale up alarm
resource "aws_autoscaling_policy" "cpu-policy" {
  name = "${var.environment}-cpu-policy"
  autoscaling_group_name = module.autoscaling.this_autoscaling_group_name
  adjustment_type = "ChangeInCapacity"
  scaling_adjustment = "1"
  cooldown = "300"
  policy_type = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "example-cpu-alarm" {
  alarm_name = "${var.environment}-cpu-alarm"
  alarm_description = "${var.environment}-cpu-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "80"
  dimensions = {
    "AutoScalingGroupName" = module.autoscaling.this_autoscaling_group_name
  }
  actions_enabled = true
  alarm_actions = ["${aws_autoscaling_policy.cpu-policy.arn}"]
}

# scale down alarm
resource "aws_autoscaling_policy" "cpu-policy-scaledown" {
  name = "${var.environment}-cpu-policy-scaledown"
  autoscaling_group_name = module.autoscaling.this_autoscaling_group_name
  adjustment_type = "ChangeInCapacity"
  scaling_adjustment = "-1"
  cooldown = "300"
  policy_type = "SimpleScaling"
}

resource "aws_cloudwatch_metric_alarm" "example-cpu-alarm-scaledown" {
  alarm_name = "${var.environment}-cpu-alarm-scaledown"
  alarm_description = "${var.environment}-cpu-alarm-scaledown"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods = "2"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  period = "120"
  statistic = "Average"
  threshold = "60"
  dimensions = {
    "AutoScalingGroupName" = module.autoscaling.this_autoscaling_group_name
  }
  actions_enabled = true
  alarm_actions = ["${aws_autoscaling_policy.cpu-policy-scaledown.arn}"]
}