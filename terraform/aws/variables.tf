variable "owner" {
  type = string
  description = "Defines the owner that this VPC "
  default = "qbr"
}

variable "environment" {
  type = string
  description = "Defines the environment that this VPC is created for"
  default = "development"
}

variable "domain_name" {
  type = string
  description = "The name of the domain the application is exposed under"
  default = "test-qb.tf"
}

variable "aws_region" {
  type = string
  description = "Defines the region that this VPC is created on"
  default = "eu-west-1"
}

variable "cidr" {
  type = string
  description = "Defines the CIDR for this VPC is created on"
  default = "10.20.0.0/16"
}

variable "private_subnets_cidr" {
  type = list
  description = "Defines the CIDR for the private subnets created for this VPC is created on"
  default = ["10.20.0.0/18", "10.20.128.0/18", "10.20.192.0/18"]
}

variable "public_subnets_cidr" {
  type = list
  description = "Defines the CIDR for the public subnets created for this VPC is created on"
  default = ["10.20.64.0/18"]
}

variable "num_of_instances" {
  type = number
  description = "The number of ec2 instances to schedule"
  default = 1
}

variable "asg_min_size" {
  type = number
  description = "The minimum number of ec2 instances on the ASG"
  default = 1
}

variable "asg_max_size" {
  type = number
  description = "The maximum number of ec2 instances on the ASG"
  default = 3
}

variable "asg_desired_capacity" {
  type = number
  description = "The number of ec2 instances running on the ASG"
  default = 1
}